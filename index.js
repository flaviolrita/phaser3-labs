const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const app = express();
const Helper = require("./lib/helper");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/", router); 
app.use(express.static('public'));


(async () => { 
    try {    
        let config = Helper.loadConfiguration(__dirname+'/.env'); 
        app.listen(config.APP_PORT);
        console.log('Server running at: '+config.APP_HOST+':'+config.APP_PORT); 
        process.exitCode = 0;
        return 0;
    }
    catch (error) {     
        console.error(error);  
        process.exitCode = -1;
        return -1;
    }    
})();

//http://localhost:8001/lab2-tutorial-src/part4.html



