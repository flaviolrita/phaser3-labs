

# Leer y realizar
- [introduccion](http://phaser.io/tutorials/getting-started-phaser3/index)
- [tutorial primer juego](http://phaser.io/tutorials/making-your-first-phaser-3-game-spanish/index)
- [tutorial juego en facebook](http://phaser.io/tutorials/getting-started-facebook-instant-games/index)


# modificar el primer juego usando mejores practicas
- [parte 1](https://blog.ourcade.co/posts/2020/make-first-phaser-3-game-modern-javascript-part1/)
- [parte 2](https://blog.ourcade.co/posts/2020/make-first-phaser-3-game-modern-javascript-part2/)
- [parte 3](https://blog.ourcade.co/posts/2020/make-first-phaser-3-game-modern-javascript-part3/)
- [parte 4](https://blog.ourcade.co/posts/2020/make-first-phaser-3-game-modern-javascript-part4/)
- [parte 5](https://blog.ourcade.co/posts/2020/make-first-phaser-3-game-modern-javascript-part5/)



# referencias
- [ejemplos](http://labs.phaser.io/) 
- [tutoriales de juegos](http://phaser.io/learn/community-tutorials)
- 